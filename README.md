# L

## Content

```
./L. R. Peloussat:
L. R. Peloussat - Regina orasului mort 1.0 '{Aventura}.docx

./L. S. Hilton:
L. S. Hilton - Maestra - V1 Maestra 1.0 '{Thriller}.docx
L. S. Hilton - Maestra - V2 Domina 1.0 '{Thriller}.docx
L. S. Hilton - Maestra - V3 Ultima 1.0 '{Thriller}.docx

./L. Tolgyesi:
L. Tolgyesi - Alibiurile inginerului Dragomir 2.0 '{SF}.docx

./L. Vlasov & D. Trifinov:
L. Vlasov & D. Trifinov - 107 povestiri despre chimie 1.0 '{Stiinta}.docx

./Lacey Dancer:
Lacey Dancer - Calitatile acestei femei 0.99 '{Dragoste}.docx
Lacey Dancer - Caprice 0.9 '{Dragoste}.docx
Lacey Dancer - Leora V1 0.8 '{Dragoste}.docx
Lacey Dancer - Leora V2 0.8 '{Dragoste}.docx
Lacey Dancer - Pasiune si scandal 0.99 '{Dragoste}.docx

./Ladislau Daradici:
Ladislau Daradici - Inaripatele din vis 0.9 '{SF}.docx
Ladislau Daradici - Luna unei nopti din luna mai 0.7 '{SF}.docx
Ladislau Daradici - Timpuri verbale 0.9 '{Diverse}.docx

./Ladislav Fuks:
Ladislav Fuks - Cazul consilierului de politie 0.9 '{Politista}.docx

./Laini Taylor:
Laini Taylor - Daughter of Smoke & Bone - V1 Nascuta din fum si os 1.0 '{SF}.docx
Laini Taylor - Daughter of Smoke & Bone - V2 Vremuri de sange 1.0 '{SF}.docx
Laini Taylor - Daughter of Smoke & Bone - V3 Visuri de ingeri si monstri 1.0 '{SF}.docx
Laini Taylor - Strange the Dreamer - V1 Visul isi alege visatorul 1.0 '{Literatura}.docx

./Lanling Xiaoxiao Sheng:
Lanling Xiaoxiao Sheng - Lotus de aur, vaza si prunisor de primavara V1 1.0 '{Chineza}.docx
Lanling Xiaoxiao Sheng - Lotus de Aur, Vaza si Prunisor de Primavara V2 1.0 '{Chineza}.docx

./Lao She:
Lao She - Dl. Nadragi 0.9 '{Diverse}.docx

./Lao Tze:
Lao Tze - Taote King 1.0 '{Spiritualitate}.docx

./Lara Avery:
Lara Avery - Cartea memoriei 0.9 '{Literatura}.docx

./Lara Prescott:
Lara Prescott - Secretele pe care nu le-am spus 1.0 '{Literatura}.docx

./La Rochefoucauld:
La Rochefoucauld - Maxime si reflectii 0.99 '{Filozofie}.docx

./Larry Bond:
Larry Bond - Phoenixul rosu 3.0 '{ActiuneComando}.docx

./Larry Brooks:
Larry Brooks - Dansul sarpelui 1.0 '{Romance}.docx

./Larry Correia:
Larry Correia - Trei Scantei 1.0 '{SF}.docx

./Larry Niven:
Larry Niven - Inelara - V1 Lumea inelara 3.0 '{SF}.docx
Larry Niven - Inelara - V2 Inginerii lumii inelare 3.0 '{SF}.docx
Larry Niven - Inelara - V3 Tronul lumii inelare 3.0 '{SF}.docx
Larry Niven - Lumea de dincolo de timp 1.0 '{SF}.docx
Larry Niven - Luna schimbatoare 0.8 '{SF}.docx
Larry Niven - Lungul brat al legii 2.0 '{SF}.docx
Larry Niven - Mostenirea Heorot - V1 Mostenirea Heorot 1.0 '{SF}.docx
Larry Niven - Mostenirea Heorot - V2 Dragonii Heorot 1.0 '{SF}.docx
Larry Niven - Omul Black Hole 1.0 '{SF}.docx
Larry Niven - Protector 0.99 '{SF}.docx

./Larry Niven & Jerry Pournelle:
Larry Niven & Jerry Pournelle - Lumea Pay - V1 Contactul 1.0 '{SF}.docx
Larry Niven & Jerry Pournelle - Lumea Pay - V2 Confruntarea 1.0 '{SF}.docx

./Lars Kepler:
Lars Kepler - Joona Linna - V1 Sub hipnoza 1.0 '{Politista}.docx
Lars Kepler - Joona Linna - V2 Pact cu diavolul 1.0 '{Politista}.docx

./Lars Saabye Christensen:
Lars Saabye Christensen - Beatles 0.7 '{Literatura}.docx
Lars Saabye Christensen - Frati pe jumatate 1.0 '{Literatura}.docx

./Lass Small:
Lass Small - Salty si Felicia 0.9 '{Dragoste}.docx

./Laura Brid:
Laura Brid - Doamna cu violete 0.99 '{Dragoste}.docx
Laura Brid - Insula 0.99 '{Dragoste}.docx
Laura Brid - Ultima escapada 0.9 '{Dragoste}.docx
Laura Brid - Un gentlemen din sud 0.9 '{Dragoste}.docx

./Laura Esquivel:
Laura Esquivel - Ca apa pentru ciocolata 0.6 '{Diverse}.docx

./Laura Hastings:
Laura Hastings - Secretul soimului 1.0 '{Thriller}.docx

./Laura Hillenbrand:
Laura Hillenbrand - De neinvins 1.0 '{ActiuneRazboi}.docx

./Laura Lippman:
Laura Lippman - Te-as recunoaste dintr-o mie 1.0 '{Suspans}.docx

./Laura Marie Altom:
Laura Marie Altom - Razbunarea Trishei 1.0 '{Romance}.docx
Laura Marie Altom - Saruta-ma din nou 1.0 '{Romance}.docx

./Laura Marshall:
Laura Marshall - Cerere de prietenie 1.0 '{Literatura}.docx

./Laura Nureldin:
Laura Nureldin - Regii timpului 2.0 '{SF}.docx

./Laura Restrepo:
Laura Restrepo - Dulce companie 0.8 '{SF}.docx

./Laura Sebastian:
Laura Sebastian - Printesa de cenusa 1.0 '{Literatura}.docx

./Laura Sorin:
Laura Sorin - Fata mosului si imparatul verde 0.99 '{Necenzurat}.docx
Laura Sorin - Moarte pe gratis 0.99 '{Diverse}.docx

./Laureen Prescott:
Laureen Prescott - Barbatul fara chip 0.9 '{Dragoste}.docx

./Laurell K. Hamilton:
Laurell K. Hamilton - Anita Blake - V1 Placeri interzise 1.0 '{Vampiri}.docx
Laurell K. Hamilton - Anita Blake - V2 Cadavrul care rade 1.0 '{Vampiri}.docx
Laurell K. Hamilton - Anita Blake - V3 Circul damnatilor 1.0 '{Vampiri}.docx
Laurell K. Hamilton - Meredith Gentry - V1 Sarutul din umbra 1.0 '{Vampiri}.docx

./Lauren Beukes:
Lauren Beukes - Fete stralucitoare 1.0 '{CalatorieinTimp}.docx

./Laurence Gardner:
Laurence Gardner - Stirpea sfantului Graal 1.0 '{Spiritualitate}.docx

./Laurence Gough:
Laurence Gough - Furtuna in desert 1.0 '{ActiuneRazboi}.docx

./Lauren Frankel:
Lauren Frankel - Adevar sau provocare 1.0 '{Literatura}.docx

./Lauren Groff:
Lauren Groff - Destin si furie 1.0 '{Romance}.docx

./Lauren Henderson:
Lauren Henderson - Femeie. Alba. Moarta 1.0 '{Politista}.docx
Lauren Henderson - Prea multe blonde 1.0 '{Politista}.docx

./Lauren Kate:
Lauren Kate - Damnare - V1 Damnare 1.0 '{Supranatural}.docx
Lauren Kate - Damnare - V2 Ratacire 0.9 '{Supranatural}.docx
Lauren Kate - Damnare - V3 Pasiune 0.7 '{Supranatural}.docx
Lauren Kate - Teardrop - V1 Lacrima 1.0 '{SF}.docx
Lauren Kate - Teardrop - V2 Cascada 1.0 '{SF}.docx

./Lauren Oliver:
Lauren Oliver - Delirium - V1 Delirium 1.0 '{SF}.docx
Lauren Oliver - Delirium - V2 Pandemonium 1.0 '{SF}.docx
Lauren Oliver - Delirium - V3 Requiem 1.0 '{SF}.docx
Lauren Oliver - O zi din sapte 1.0 '{Literatura}.docx
Lauren Oliver - Panica 1.0 '{SF}.docx
Lauren Oliver - Replica Gemma 1.0 '{SF}.docx
Lauren Oliver - Replica Lyra 1.0 '{SF}.docx

./Laurent 6aude:
Laurent 6aude - Soarele neamului Scorta 0.8 '{Diverse}.docx

./Laurent Gounelle:
Laurent Gounelle - Dumnezeu calatoreste intotdeauna incognito 1.0 '{Literatura}.docx
Laurent Gounelle - Omul care dorea sa fie fericit 0.99 '{Spiritualitate}.docx
Laurent Gounelle - Ziua in care am invatat sa traiesc 1.0 '{Literatura}.docx

./Laurent Graff:
Laurent Graff - O singura fotografie 0.99 '{Literatura}.docx

./Laurentiu Budau:
Laurentiu Budau - Harbuzzia 0.9 '{Teatru}.docx

./Laurentiu Cernaianu:
Laurentiu Cernaianu - Meniuri si retete pentru pitici 1.0 '{Copii}.docx
Laurentiu Cernaianu - Retete culinare 0-3 ani 0.9 '{Copii}.docx

./Laurentiu Demetrovici:
Laurentiu Demetrovici - Planeta gigantilor 0.99 '{SF}.docx
Laurentiu Demetrovici - Punct de vedere 0.99 '{SF}.docx

./Laurentiu Ene:
Laurentiu Ene - Umbrele ucigase 0.9 '{Diverse}.docx

./Laurentiu Fulga:
Laurentiu Fulga - Alexandra si infernul 0.9 '{Dragoste}.docx
Laurentiu Fulga - E noapte si e frig seniori 0.7 '{Diverse}.docx

./Laurentiu Mihaileanu:
Laurentiu Mihaileanu - Dreptul la iarba 1.0 '{Diverse}.docx

./Laurentiu Nistorescu:
Laurentiu Nistorescu - Nocturnaliile despre solstitiu 0.9 '{Diverse}.docx
Laurentiu Nistorescu - Petrecand prin rasplaneta 0.9 '{SF}.docx
Laurentiu Nistorescu - Sa-ti spun eu cum se face 0.99 '{SF}.docx
Laurentiu Nistorescu - Si nu-si va da vinul via verde pe grui 0.9 '{SF}.docx

./Lauren Weisberger:
Lauren Weisberger - Razbunarea se imbraca de la Prada 0.99 '{Literatura}.docx

./Lauren Willig:
Lauren Willig - Sotia engleza 1.0 '{Politista}.docx

./Laurey Bright:
Laurey Bright - Dupa sapte ani 0.99 '{Dragoste}.docx
Laurey Bright - Farmecul soimului 0.9 '{Dragoste}.docx
Laurey Bright - Sub vraja iubirii 0.99 '{Dragoste}.docx

./Laurian Cnobloch:
Laurian Cnobloch - Incantatie 0.9 '{SF}.docx

./Laurie Ford:
Laurie Ford - Drumul care duce in trecut 0.99 '{Dragoste}.docx
Laurie Ford - Mostenire si visuri 0.99 '{Romance}.docx

./Laurie Paige:
Laurie Paige - Dupa ploaie soare 0.9 '{Romance}.docx

./Lavie Tidhar:
Lavie Tidhar - Statia centrala 1.0 '{SF}.docx

./Lawrence Alexander:
Lawrence Alexander - Rubicon 1.0 '{Thriller}.docx

./Lawrence Block:
Lawrence Block - Matthew Scudder - V6 Cand sfanta crasma se inchide 1.0 '{Politista}.docx
Lawrence Block - Matthew Scudder - V9 Dans la abator 1.0 '{Politista}.docx
Lawrence Block - Opt milioane de feluri de a muri 0.99 '{Politista}.docx
Lawrence Block - Umbland printre morminte 1.0 '{Thriller}.docx

./Lawrence Durrell:
Lawrence Durrell - Cvartetul din Alexandria - V1 Justine 1.0 '{AventuraIstorica}.docx
Lawrence Durrell - Cvartetul din Alexandria - V2 Balthazar 1.0 '{AventuraIstorica}.docx
Lawrence Durrell - Cvartetul din Alexandria - V3 Mountolive 1.0 '{AventuraIstorica}.docx
Lawrence Durrell - Cvartetul din Alexandria - V4 Clea 1.0 '{AventuraIstorica}.docx
Lawrence Durrell - Labirintul intunecat 0.9 '{AventuraIstorica}.docx

./Lawrence Joseph:
Lawrence Joseph - Apocalipsa 2012 - V1 Apocalipsa 1.0 '{MistersiStiinta}.docx
Lawrence Joseph - Apocalipsa 2012 - V2 Ce va fi dupa 1.0 '{MistersiStiinta}.docx

./Lawrence Krauss:
Lawrence Krauss - Universul din nimic 1.0 '{MistersiStiinta}.docx

./Lawrence Norfolk:
Lawrence Norfolk - Dictionarul lui Lempriere V1,2 1.0 '{Literatura}.docx

./Lawrence Sanders:
Lawrence Sanders - V1 Reversul medaliei 1.0 '{Politista}.docx
Lawrence Sanders - V2 Secretul lui Mcnally 1.0 '{Politista}.docx
Lawrence Sanders - V3 Norocul lui Mcnally 1.0 '{Politista}.docx

./Lea Jacobson:
Lea Jacobson - Floare de noapte, floare de bar 1.0 '{Erotic}.docx

./Leanne Banks:
Leanne Banks - Dansand cu diavolul 1.0 '{Romance}.docx
Leanne Banks - Pentru placerea altetei sale regale 1.0 '{Romance}.docx

./Lee Child:
Lee Child - Ghinioane si incurcaturi 0.99 '{Suspans}.docx
Lee Child - Jack Reacher - V1 Capcana Margrave 3.0 '{Suspans}.docx
Lee Child - Jack Reacher - V2 Evadare imposibila 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V6 Sa nu gresesti 3.0 '{Suspans}.docx
Lee Child - Jack Reacher - V7 Persuader 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V10 Nimic nu e usor 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V11 Belele si ghinioane 1.0 '{Suspans}.docx
Lee Child - Jack Reacher - V12 Nimic de pierdut 1.0 '{Suspans}.docx
Lee Child - Jack Reacher - V14 61 de ore 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V15 Merita sa mori 1.0 '{Suspans}.docx
Lee Child - Jack Reacher - V16 Ancheta 3.0 '{Suspans}.docx
Lee Child - Jack Reacher - V17 Urmarit 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V18 Sa nu te intorci niciodata 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V19 Personal 2.0 '{Suspans}.docx
Lee Child - Jack Reacher - V20 Convinge-ma 1.0 '{Suspans}.docx
Lee Child - Jack Reacher - V21 Scoala de noapte 1.0 '{Suspans}.docx
Lee Child - Jack Reacher - V22 Filiera de la miezul noptii 1.0 '{Suspans}..docx

./Lee Daniels:
Lee Daniels - Eu si inima mea 0.99 '{Romance}.docx
Lee Daniels - Reciful ascuns 0.99 '{Dragoste}.docx

./Lee Parker:
Lee Parker - Logodna fictiva 0.99 '{Romance}.docx
Lee Parker - Spirit aventuros 0.99 '{Romance}.docx

./Lee Williams:
Lee Williams - Oriunde si intotdeauna 0.9 '{Dragoste}.docx

./Leif G. W. Persson:
Leif G. W. Persson - Everett Backstrom - V1 Cazul Linda 1.0 '{Politista}.docx
Leif G. W. Persson - Everett Backstrom - V2 Ucigasul dragonului 1.0 '{Politista}.docx
Leif G. W. Persson - Everett Backstrom - V3 Pinocchio 1.0 '{Politista}.docx

./Leif Panduro:
Leif Panduro - Ferestrele 0.8 '{Literatura}.docx

./Leigh Bardugo:
Leigh Bardugo - Banda celor Sase Ciori - V1 Banda celor sase ciori 1.0 '{Aventura}.docx
Leigh Bardugo - Grisha - V1 Regatul umbrelor 1.0 '{SF}.docx
Leigh Bardugo - Grisha - V2 Regatul furtunilor 1.0 '{SF}.docx
Leigh Bardugo - Grisha - V3 Regatul luminilor 1.0 '{SF}.docx
Leigh Bardugo - Grisha - V4 Regele Cicatricilor 1.0 '{SF}.docx

./Leila:
Leila - Maritata cu forta 1.0 '{Literatura}.docx

./Leila Meacham:
Leila Meacham - Trandafiri V1 0.9 '{Dragoste}.docx
Leila Meacham - Trandafiri V2 0.8 '{Dragoste}.docx

./Leila Slimani:
Leila Slimani - Cantec lin 1.0 '{Literatura}.docx
Leila Slimani - In gradina capcaunului 1.0 '{Literatura}.docx

./Lemerle Paul:
Lemerle Paul - Istoria Bizantului 0.6 '{Istorie}.docx

./Lemony Snicket:
Lemony Snicket - Evenimente nefericite - V1 Inceput indoliat 1.0 '{Literatura}.docx

./Lena Constante:
Lena Constante - Evadare tacuta 1.0 '{Politica}.docx

./Len Deighton:
Len Deighton - Bombardierul V1 1.0 '{ActiuneRazboi}.docx
Len Deighton - Bombardierul V2 1.0 '{ActiuneRazboi}.docx

./Leni Jalabier:
Leni Jalabier - Noi, zeii 0.9 '{Diverse}.docx
Leni Jalabier - Nuca 0.9 '{Diverse}.docx

./Len Kasten:
Len Kasten - Istoria secreta a extraterestrilor. Interventii de natura extraterestra in armata, guvernare si tehnologie 0.99 '{MistersiStiinta}.docx

./Leo Angart:
Leo Angart - Recuperati-va vederea in mod natural cu Leo Angart 1.0 '{Sanatate}.docx
Leo Angart - Sa ne imbunatatim vederea in mod natural 0.5 '{Sanatate}.docx

./Leonard Azanfirei:
Leonard Azanfirei - E bine sa stii! 0.7 '{Sanatate}.docx

./Leonard Oprea:
Leonard Oprea - Camasa de forta 0.7 '{Politica}.docx
Leonard Oprea - Cele noua invataturi ale lui Theophil Magus despre Magia Transilvana 1.0 '{Spiritualitate}.docx
Leonard Oprea - Domenii interzise 0.9 '{SF}.docx
Leonard Oprea - Meditatiile lui Theophil Magus sau simple cugetari crestine la inceput de mileniu 3 0.9 '{Spiritualitate}.docx

./Leon de Winter:
Leon de Winter - Savantul si traficantii 1.0 '{Suspans}.docx

./Leonida Neamtu:
Leonida Neamtu - Acolo unde vantul rostogoleste norii V1 3.0 '{Aventura}.docx
Leonida Neamtu - Acolo unde vantul rostogoleste norii V2 3.0 '{Aventura}.docx
Leonida Neamtu - Aventura si Contraaventura 0.99 '{Aventura}.docx
Leonida Neamtu - Balada capitanului Haag 1.0 '{Politista}.docx
Leonida Neamtu - Blondul impotriva umbrei sale 1.0 '{SF}.docx
Leonida Neamtu - Casa Isolda sau cutremurul 0.99 '{Aventura}.docx
Leonida Neamtu - Celalalt prezent 1.0 '{Aventura}.docx
Leonida Neamtu - Chirie pentru speranta 1.0 '{Aventura}.docx
Leonida Neamtu - Curbura dubla a infinitului 1.0 '{Aventura}.docx
Leonida Neamtu - Deodata, inevitabilul 1.0 '{Aventura}.docx
Leonida Neamtu - Frumusetea pietrei nevazute 1.1 '{Aventura}.docx
Leonida Neamtu - Inotatorul ranit 1.0 '{Politista}.docx
Leonida Neamtu - Intoarcerea Focului 1.0 '{Aventura}.docx
Leonida Neamtu - La Goulue danseaza cu Chocolat 0.99 '{Aventura}.docx
Leonida Neamtu - Moartea ca o floare de nu-ma-uita 1.0 '{Politista}.docx
Leonida Neamtu - Naratiuni detective 1.0 '{Politista}.docx
Leonida Neamtu - Ochii 1.0 '{Poezie}.docx
Leonida Neamtu - Orhidee pentru Marifelis 1.0 '{Aventura}.docx
Leonida Neamtu - Speranta pentru marele vis 1.0 '{Aventura}.docx
Leonida Neamtu - Speranta pentru speranta 1.0 '{Aventura}.docx
Leonida Neamtu - Strania poveste a marelui joc 1.0 '{Aventura}.docx
Leonida Neamtu - Teroare pentru colonel 1.0 '{Aventura}.docx
Leonida Neamtu - Terra Orr 1.0 '{Aventura}.docx
Leonida Neamtu - Toporul de argint 0.99 '{Aventura}.docx

./Leonid Borisov:
Leonid Borisov - Calul de sah merge cotit 1.0 '{Tineret}.docx

./Leonid Dimov:
Leonid Dimov - ABC 0.6 '{Versuri}.docx

./Leonid Leonov:
Leonid Leonov - Bursucii 1.0 '{Diverse}.docx

./Leonid Petrescu:
Leonid Petrescu - A 12-a varianta 0.8 '{SF}.docx
Leonid Petrescu - Autobuzul profesorului 1.0 '{SF}.docx
Leonid Petrescu - Cearta furtunilor 0.9 '{SF}.docx
Leonid Petrescu - Nu cautati eroul 1.0 '{SF}.docx

./Leonid Sobolev:
Leonid Sobolev - Sub vulturii imperiali 1.0 '{Istoric}.docx

./Leonid Zorin:
Leonid Zorin - Un barbat si mai multe femei 0.99 '{Teatru}.docx

./Leon Magdan:
Leon Magdan - Pilde si povestiri ortodoxe cu talc 0.99 '{Spiritualitate}.docx

./Leo Perutz:
Leo Perutz - Calaretul suedez 0.9 '{AventuraIstorica}.docx

./Leopoldo Garcia:
Leopoldo Garcia - Pasiunea Annei Ozores V1-2 1.0 '{Dragoste}.docx

./Leo Serban Alex:
Leo Serban Alex - Cu stiloul pe pelicula 0.9 '{Diverse}.docx

./Leo Taxil:
Leo Taxil - Biblia hazlie 1.0 '{Umor}.docx
Leo Taxil - Viata lui Iisus 2.0 '{Filozofie}.docx

./Lesley Downer:
Lesley Downer - Curtezana si samuraiul 0.9 '{Literatura}.docx
Lesley Downer - Ultima concubina 1.0 '{Literatura}.docx

./Leslie Charteris:
Leslie Charteris - Sfantul contra tigrului 1.0 '{Politista}.docx
Leslie Charteris - Sfantul la Londra 1.0 '{Politista}.docx
Leslie Charteris - Sfantul la New York 1.0 '{Politista}.docx
Leslie Charteris - Sus mainile Simon Templar 1.0 '{Politista}.docx

./Leslie Knowles:
Leslie Knowles - O lume a tacerii 1.0 '{Romance}.docx

./Leslie Lafoy:
Leslie Lafoy - Logodnica disparuta 0.99 '{Dragoste}.docx
Leslie Lafoy - Sa uitam trecutul 0.99 '{Dragoste}.docx

./Lester del Rey:
Lester del Rey - Cantec de asfintit 1.0 '{SF}.docx
Lester del Rey - Chiar de mor visatorii 1.0 '{SF}.docx

./Letty Ingram:
Letty Ingram - Ratacind printre vise 0.99 '{Dragoste}.docx

./Lev Grossman:
Lev Grossman - Codex 1.0 '{Supranatural}.docx
Lev Grossman - Magicienii - V1 Magicienii 1.0 '{Supranatural}.docx
Lev Grossman - Magicienii - V2 Regele magician 1.0 '{Supranatural}.docx
Lev Grossman - Magicienii - V3 Taramul magicianului 1.0 '{Supranatural}.docx

./Lev Nicolaevici Tolstoi:
Lev Nicolaevici Tolstoi - Anna Karenina 2.0 '{ClasicSt}.docx
Lev Nicolaevici Tolstoi - Furnica si porumbita 0.99 '{ClasicSt}.docx
Lev Nicolaevici Tolstoi - Razboi si pace V1-4 2.0 '{ClasicSt}.docx

./Lev Nikolaevici Tolstoi:
Lev Nikolaevici Tolstoi - Invierea 1.0 '{ClasicSt}.docx
Lev Nikolaevici Tolstoi - Moartea lui Ivan Ilici 2.3 '{ClasicSt}.docx
Lev Nikolaevici Tolstoi - Sonata Kreutzer. Parintele Serghi. Hagi Murad 1.0 '{ClasicSt}.docx

./Lev Uspenski:
Lev Uspenski - Salmugra 0.99 '{Diverse}.docx

./Lewis Carroll:
Lewis Carroll - Alice in tara minunilor 1.0 '{SF}.docx

./Lewis Padgett:
Lewis Padgett - Frazbile si granchioase 0.99 '{SF}.docx

./Lewis Shiner:
Lewis Shiner - Orasul alb 0.99 '{SF}.docx

./Lewis Wallace:
Lewis Wallace - Ben Hur V1,2 3.0 '{AventuraIstorica}.docx

./Lia Bugnar & Vali Florescu:
Lia Bugnar & Vali Florescu - Cobai 0.8 '{Teatru}.docx

./Liana Mares:
Liana Mares - Bleak House 0.9 '{SF}.docx

./Liane Moriarty:
Liane Moriarty - Cand vina ne desparte 1.0 '{Literatura}.docx
Liane Moriarty - Marile minciuni nevinovate 1.0 '{Literatura}.docx
Liane Moriarty - Secretul sotului 1.0 '{Literatura}.docx

./Lian Hearn:
Lian Hearn - Otori - V1 Sa nu trezesti podeaua privighetoare 1.0 '{Supranatural}.docx
Lian Hearn - Otori - V2 Sub cerul liber avand drept perna iarba 1.0 '{Supranatural}.docx

./Licia Troisi:
Licia Troisi - Cronicile Lumii Pamantene - V1 Nihal de pe pamantul vantului 2.0 '{Supranatural}.docx
Licia Troisi - Cronicile Lumii Pamantene - V2 Misiunea lui Sennar 2.0 '{Supranatural}.docx
Licia Troisi - Cronicile Lumii Pamantene - V3 Talismanul puterii 2.0 '{Supranatural}.docx
Licia Troisi - Cronicile Lumii Pamantene - V4 Povestile pierdute 1.0 '{Supranatural}.docx
Licia Troisi - Razboaiele Lumii Pamantene - V1 Secta asasinilor 1.0 '{Supranatural}.docx
Licia Troisi - Razboaiele Lumii Pamantene - V2 Cele doua razboinice 1.0 '{Supranatural}.docx
Licia Troisi - Razboaiele Lumii Pamantene - V3 Un Nou Regat 1.0 '{Supranatural}.docx

./Lidia Handabura:
Lidia Handabura - Povestiri din anul cometei 0.9 '{Diverse}.docx

./Li Hongzhi:
Li Hongzhi - Falun Gong 0.8 '{Spiritualitate}.docx

./Liliana David:
Liliana David - Holisticromii 0.99 '{SF}.docx
Liliana David - Nisa 2.0 '{SF}.docx
Liliana David - Toboganul 2.0 '{SF}.docx

./Liliana Lazar:
Liliana Lazar - Pamantul oamenilor liberi 1.0 '{Literatura}.docx

./Lilian Darcy:
Lilian Darcy - Infirmiera din Noua Caledonie 0.9 '{Dragoste}.docx

./Lilia Seven:
Lilia Seven - Mesagerii luminii 0.7 '{Diverse}.docx

./Lili St. Crow:
Lili St. Crow - Altfel de Ingeri - V1 Altfel de ingeri 1.0 '{Vampiri}.docx
Lili St. Crow - Altfel de Ingeri - V2 Tradari 0.9 '{Vampiri}.docx
Lili St. Crow - Altfel de Ingeri - V3 Gelozie 0.9 '{Vampiri}.docx

./Lilli Promet:
Lilli Promet - Primavera 1.0 '{Dragoste}.docx

./Lily Stevens:
Lily Stevens - Inima lui Rachel 0.99 '{Dragoste}.docx
Lily Stevens - Intre doi barbati 0.9 '{Romance}.docx

./Lina Biraescu:
Lina Biraescu - Perpetuum mobile 0.8 '{Diverse}.docx

./Lincoln Child:
Lincoln Child - Infernul de gheata 1.0 '{Aventura}.docx
Lincoln Child - Inima furtunii 1.0 '{Aventura}.docx

./Linda Adams:
Linda Adams - Cea care aduce fericirea 0.99 '{Dragoste}.docx
Linda Adams - Lungul drum spre casa 1.0 '{Romance}.docx
Linda Adams - Salvatorul misterios 1.0 '{Romance}.docx
Linda Adams - Sarmana fata bogata 1.0 '{Romance}.docx
Linda Adams - Secretul lui Joe Elliot 1.0 '{Romance}.docx

./Linda Cajio:
Linda Cajio - Aleea florilor 0.9 '{Romance}.docx
Linda Cajio - Fratele vitreg 0.99 '{Dragoste}.docx
Linda Cajio - Partida perfecta 0.9 '{Dragoste}.docx
Linda Cajio - Un strain irezistibil 0.99 '{Dragoste}.docx

./Linda Cornwall:
Linda Cornwall - Umbre si lumini 0.2 '{Dragoste}.docx

./Linda David:
Linda David - Chemarea desertului 1.0 '{Romance}.docx
Linda David - Demon deghizat 0.2 '{Dragoste}.docx

./Linda Howard:
Linda Howard - Capcana 1.0 '{Thriller}.docx
Linda Howard - Intre dorinta si pericol 1.0 '{Thriller}.docx
Linda Howard - Jocul mortii 1.0 '{Thriller}.docx
Linda Howard - Mackenzie Family - V1 O inima neimblanzita 1.0 '{Thriller}.docx
Linda Howard - Mackenzie Family - V2 Alegerea inimii 1.0 '{Thriller}.docx
Linda Howard - Mackenzie Family - V3 O noapte de neuitat 1.0 '{Thriller}.docx
Linda Howard - Mackenzie Family - V4 Placere exclusiva 1.0 '{Thriller}.docx
Linda Howard - Mackenzie Family - V5 Un joc de noroc 1.0 '{Thriller}.docx

./Linda Hudson:
Linda Hudson - Meseria de actrita 0.9 '{Romance}.docx

./Linda Hughes:
Linda Hughes - Operatiunea impacarea 0.9 '{Dragoste}.docx

./Linda Lael Miller:
Linda Lael Miller - O nevasta incomoda 1.0 '{Romance}.docx
Linda Lael Miller - Sirena cu ochi de aur 1.0 '{Romance}.docx

./Linda Snow:
Linda Snow - Ruda saraca 0.99 '{Dragoste}.docx

./Linda Warren:
Linda Warren - Un cowboy indragostit 1.0 '{Romance}.docx

./Lindsay Jayne Ashford:
Lindsay Jayne Ashford - Femeia din Orient Express 1.0 '{Politista}.docx

./Lino Aldani:
Lino Aldani - 37 de grade 0.9 '{SF}.docx
Lino Aldani - Crucea de gheata 1.0 '{SF}.docx
Lino Aldani - Eclipsa 2000 1.0 '{SF}.docx
Lino Aldani - Malul celalalt 0.99 '{SF}.docx
Lino Aldani - Noapte buna, Sofia 1.0 '{SF}.docx
Lino Aldani - Vanatorul electronic 1.0 '{SF}.docx

./Linwood Barclay:
Linwood Barclay - No Time for Goodbye - V1 Nu-i timp de bun ramas 1.0 '{Literatura}.docx

./Lion Feuchtwanger:
Lion Feuchtwanger - Falsul Nero 1.0 '{Literatura}.docx

./Lisa Ballantyne:
Lisa Ballantyne - Vinovatul 1.0 '{Thriller}.docx

./Lisa Crawford:
Lisa Crawford - Barbatul de gheata 0.99 '{Romance}.docx
Lisa Crawford - Orhideea alba 0.9 '{Dragoste}.docx

./Lisa Dale:
Lisa Dale - Magia dragostei 0.9 '{Dragoste}.docx

./Lisa Gardner:
Lisa Gardner - Vecinul 1.0 '{Thriller}.docx

./Lisa Genova:
Lisa Genova - Altfel... si totusi Alice 1.0 '{Literatura}.docx

./Lisa J. Smith:
Lisa J. Smith - Cercul Secret - V1 Ritualul 1.0 '{Vampiri}.docx
Lisa J. Smith - Cercul Secret - V2 Captiva 1.0 '{Vampiri}.docx
Lisa J. Smith - Cercul Secret - V3 Puterea 1.0 '{Vampiri}.docx
Lisa J. Smith - Cercul Secret - V4 Despartirea 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele lui Stefan - V1 Inceputurile 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V1 Trezirea 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V2 Lupta 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V3 Furia 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V4 Reuniunea 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V5 Intoarcerea - Caderea noptii 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V6 Intoarcerea - Suflete umbra 1.0 '{Vampiri}.docx
Lisa J. Smith - Jurnalele Vampirilor - V7 Intoarcerea - Miez de noapte 1.0 '{Vampiri}.docx

./Lisa Jackson:
Lisa Jackson - Iubire vinovata 1.0 '{Romance}.docx

./Lisa Kleypas:
Lisa Kleypas - Diavolul cu ochi albastri 0.99 '{Dragoste}.docx
Lisa Kleypas - Diavolul in iarna 0.7 '{Dragoste}.docx
Lisa Kleypas - Dragoste si mandrie 2.0 '{Dragoste}.docx
Lisa Kleypas - Hathaway - V3 Iubeste-ma in fiecare seara 1.0 '{Dragoste}.docx
Lisa Kleypas - Hathaway - V5 Pana dimineata casatoriti 0.6 '{Dragoste}.docx
Lisa Kleypas - Numai in bratele tale 2.0 '{Dragoste}.docx
Lisa Kleypas - Pentru ca esti a mea 1.0 '{Dragoste}.docx
Lisa Kleypas - Roscata cu ochii negri 1.0 '{Dragoste}.docx
Lisa Kleypas - Scandal in primavara 0.7 '{Dragoste}.docx
Lisa Kleypas - Wallflower - V1 Secretul unei nopti de vara 1.0 '{Dragoste}.docx
Lisa Kleypas - Wallflower - V2 S-a intamplat intr-o toamna 1.0 '{Dragoste}.docx

./Lisa Plumley:
Lisa Plumley - Alegere dificila 0.9 '{Dragoste}.docx

./Lisa Randal:
Lisa Randal - Atingerea dragostei 0.9 '{Dragoste}.docx
Lisa Randal - Senina este ziua 0.9 '{Romance}.docx

./Lisa Sanders:
Lisa Sanders - Fiecare pacient spune o poveste 1.0 '{Sanatate}.docx

./Lisa See:
Lisa See - Fetele din Shanghai 1.0 '{Literatura}.docx
Lisa See - Floare de zapada si evantaiul secret 1.0 '{Diverse}.docx
Lisa See - Visul lui Joy 0.7 '{Literatura}.docx

./Lisa Stomme:
Lisa Stomme - Fata cu fragi 0.99 '{Literatura}.docx

./Lisa Tuttle:
Lisa Tuttle - O astronava de piatra 0.99 '{SF}.docx

./Lisa Unger:
Lisa Unger - Ridley Jones - V1 Minciuni periculoase 1.0 '{Thriller}.docx
Lisa Unger - Ridley Jones - V2 Adevarul care ucide 1.0 '{Thriller}.docx

./Lisa Wingate:
Lisa Wingate - Inainte sa fim ai vostri 1.0 '{Literatura}.docx

./Lise Bourbeau:
Lise Bourbeau - Asculta, mananca si inceteaza sa te controlezi 0.8 '{DezvoltarePersonala}.docx
Lise Bourbeau - Asculta-ti corpul, prietenul cel mai bun de pe pamant 0.8 '{DezvoltarePersonala}.docx
Lise Bourbeau - Cele 5 rani care ne impiedica sa fim noi insine 0.7 '{DezvoltarePersonala}.docx
Lise Bourbeau - Corpul tau iti spune iubeste-te 0.5 '{DezvoltarePersonala}.docx
Lise Bourbeau - Stii cine esti 0.7 '{DezvoltarePersonala}.docx

./Lisette Larkins:
Lisette Larkins - De vorba cu extraterestrii 1.0 '{MistersiStiinta}.docx

./Lissa Price:
Lissa Price - Enders 1.0 '{SF}.docx

./Literatura Japoneza:
Literatura Japoneza - Trei cazuri 1.0 '{Politista}.docx

./Liternautica:
Liternautica - Carlinga mea draga 1.0 '{SF}.docx

./Liuben Dilov:
Liuben Dilov - Drumul lui Icar 1.0 '{SF}.docx

./Liu Cixin:
Liu Cixin - Amintiri din trecutul Terrei - V1 Problema celor trei corpuri 1.0 '{SF}.docx
Liu Cixin - Amintiri din trecutul Terrei - V2 Padurea intunecata 1.0 '{SF}.docx

./Liu Zhenyun:
Liu Zhenyun - Nu mi-am omorat barbatul 1.0 '{Literatura}.docx

./Livia Ardelean:
Livia Ardelean - Doctore, cine a murit 1.0 '{Politista}.docx

./Livia Ciobanu:
Livia Ciobanu - O altfel de poveste cu fantome 0.99 '{Spiritualitate}.docx
Livia Ciobanu - Poveste fara sfarsit 0.99 '{SF}.docx
Livia Ciobanu - Zodia cainelui de metal 0.99 '{SF}.docx

./Liviu Braicu:
Liviu Braicu - Ancheta asupra unui Dumnezeu local 0.99 '{Politista}.docx

./Liviu Branzas:
Liviu Branzas - Raza din catacomba 0.9 '{Politica}.docx

./Liviu Gogu:
Liviu Gogu - La pomana electorala 0.9 '{Politica}.docx

./Liviu Iancu:
Liviu Iancu - Xanax 1.0 '{Necenzurat}.docx

./Liviu Ioan Stoiciu:
Liviu Ioan Stoiciu - Undeva, la sud-est 1.0 '{Diverse}.docx

./Liviu Papadima:
Liviu Papadima - Comediile lui I. L. Caragiale 0.99 '{Critica}.docx

./Liviu Radu:
Liviu Radu - Amintiri despre inceputuri 0.9 '{SF}.docx
Liviu Radu - Cazul Iov 1.0 '{SF}.docx
Liviu Radu - Descendenti 0.8 '{SF}.docx
Liviu Radu - Infruntarea nemuritorilor 1.0 '{SF}.docx
Liviu Radu - Intre cer si pamant 1.0 '{SF}.docx
Liviu Radu - Loteria 0.99 '{SF}.docx
Liviu Radu - Lumina trebuie sa vina dinspre rasarit 0.99 '{SF}.docx
Liviu Radu - Mesagerul 0.99 '{SF}.docx
Liviu Radu - Mesaj inca nedescoperit 0.99 '{SF}.docx
Liviu Radu - Primul pas 0.99 '{SF}.docx
Liviu Radu - Stella 0.8 '{SF}.docx
Liviu Radu - Sub semnul lui unsprezece 0.99 '{SF}.docx
Liviu Radu - Taravik - V1 Armata moliilor 1.0 '{SF}.docx
Liviu Radu - Taravik - V2 La galop prin piramida 1.0 '{SF}.docx
Liviu Radu - Taravik - V3 Infruntarea nemuritorilor 1.0 '{SF}.docx

./Liviu Rebreanu:
Liviu Rebreanu - Adam si Eva 2.0 '{ClasicRo}.docx
Liviu Rebreanu - Amandoi 0.9 '{ClasicRo}.docx
Liviu Rebreanu - Calvarul 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Ciuleandra 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Craisorul Horia 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Glasul inimii 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Ion 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Jar 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Opere - V2 Nuvele 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Opere - V3 Nuvele 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Opere - V7 Ciuleandra. Craisorul Horia 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Opere - V9 Jar. Amandoi 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Padurea spanzuratilor 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Plicul 1.0 '{ClasicRo}.docx
Liviu Rebreanu - Rascoala 5.0 '{ClasicRo}.docx

./Liza Dalby:
Liza Dalby - Povestea doamnei Murasaki 1.0 '{Literatura}.docx

./Liza Marklund:
Liza Marklund - Explozii in Stockholm 2.0 '{Suspans}.docx
Liza Marklund - Fundatia Paradis 1.0 '{Suspans}.docx
Liza Marklund - Lupul rosu 1.0 '{Literatura}.docx
Liza Marklund - Prime Time 2.0 '{Suspans}.docx
Liza Marklund - Studio 69 1.0 '{Suspans}.docx
Liza Marklund - Testamentul lui Nobel 1.0 '{Politista}.docx

./Lizbie Browne:
Lizbie Browne - Cei mai buni dusmani 1.0 '{Dragoste}.docx

./Liz Braswell:
Liz Braswell - Cele 9 Vieti ale lui Chloe King - V1 Caderea 1.0 '{Supranatural}.docx
Liz Braswell - Cele 9 Vieti ale lui Chloe King - V2 Rapirea 1.0 '{Supranatural}.docx

./Lloyd C. Douglas:
Lloyd C. Douglas - Camasa lui Cristos V1 0.8 '{AventuraIstorica}.docx
Lloyd C. Douglas - Camasa lui Cristos V2 2.0 '{AventuraIstorica}.docx
Lloyd C. Douglas - Marele pescar 1.0 '{AventuraIstorica}.docx
Lloyd C. Douglas - Obstacole 1.0 '{Literatura}.docx
Lloyd C. Douglas - Parada 0.6 '{Literatura}.docx

./Lois Lowry:
Lois Lowry - Darul lui Jonas 1.0 '{SF}.docx
Lois Lowry - Numara stelele 1.0 '{Tineret}.docx
Lois Lowry - The Giver - V2 Fauritoarea de albastru 1.0 '{SF}.docx

./Lois Mcmaster Bujold:
Lois Mcmaster Bujold - Vorkosigan - V1 Blestemul Chalionului 1.0 '{SF}.docx
Lois Mcmaster Bujold - Vorkosigan - V2 Paladinul sufletelor 1.0 '{SF}.docx
Lois Mcmaster Bujold - Vorkosigan - V3 Vanatoarea sfanta 1.0 '{SF}.docx
Lois Mcmaster Bujold - Vorkosigan - V4 Cioburi de onoare 1.0 '{SF}.docx
Lois Mcmaster Bujold - Vorkosigan - V5 Dans in oglinda 2.0 '{SF}.docx
Lois Mcmaster Bujold - Vorkosigan - V6 Barrayar 1.0 '{SF}.docx

./Lois Testerman:
Lois Testerman - Revansa amara 0.9 '{Romance}.docx

./Lola Beccaria:
Lola Beccaria - Dezgolita 1.0 '{Erotic}.docx

./Lola Stere Chiracu:
Lola Stere Chiracu - Pribeagul 0.5 '{IstoricaRo}.docx
Lola Stere Chiracu - Prizonierul de la Heracleea 1.1 '{IstoricaRo}.docx

./Lope de Vega:
Lope de Vega - Cainele gradinarului 1.0 '{Teatru}.docx
Lope de Vega - Cavalerul din Olmedo 1.0 '{Teatru}.docx
Lope de Vega - Comedii 1.0 '{Teatru}.docx
Lope de Vega - Fantana turmelor 1.0 '{Teatru}.docx
Lope de Vega - Fata cu ulciorul 1.0 '{Teatru}.docx
Lope de Vega - Mofturile Belisei 1.0 '{Teatru}.docx
Lope de Vega - Paribanez si Comandorul din Ocana 1.0 '{Teatru}.docx
Lope de Vega - Pentru altii proasta, pentru ea desteapta 1.0 '{Teatru}.docx
Lope de Vega - Steaua Sevillei 1.0 '{Teatru}.docx
Lope de Vega - Vitejiile Belisei 1.0 '{Teatru}.docx

./Lord Dunsany:
Lord Dunsany - Mireasa centaurului 0.8 '{SF}.docx
Lord Dunsany - Venirea marii 0.8 '{SF}.docx

./Lorena Stoica:
Lorena Stoica - Divanul gheiselor 0.9 '{Diverse}.docx

./Lorenzo Marone:
Lorenzo Marone - Maine poate am sa raman 1.0 '{Literatura}.docx
Lorenzo Marone - Tentatia de a fi fericit 1.0 '{Literatura}.docx

./Lori Herter:
Lori Herter - De ce ai fugit de dragostea mea 0.99 '{Romance}.docx

./Lori Reid:
Lori Reid - Elemente de chiromantie 0.9 '{MistersiStiinta}.docx

./Lothar Gruchmann:
Lothar Gruchmann - Al doilea razboi mondial - Strategie si politica 0.6 '{Razboi}.docx

./Louella Cramer:
Louella Cramer - Conspiratia tacerii 0.99 '{Dragoste}.docx
Louella Cramer - Sfarsitul cosmarului 0.99 '{Dragoste}.docx

./Louisa M. Alcott:
Louisa M. Alcott - Fiicele Dr. March 0.99 '{Literatura}.docx

./Louis Aragon:
Louis Aragon - Saptamana patimilor 1.0 '{Literatura}.docx

./Louis Boussenard:
Louis Boussenard - Milioanele lui Vulpe Rosie 1.0 '{Tineret}.docx

./Louis Bromfield:
Louis Bromfield - Vin ploile 1.0 '{ClasicSt}.docx

./Louis Charles Royer:
Louis Charles Royer - Amanta neagra 1.0 '{Dragoste}.docx

./Louise L. Hay:
Louise L. Hay - Cauzele spirituale ale bolilor 0.9 '{Spiritualitate}.docx
Louise L. Hay - Meditatii pentru a-ti vindeca viata 0.2 '{Spiritualitate}.docx
Louise L. Hay - Poti sa-ti vindeci viata 0.7 '{Spiritualitate}.docx
Louise L. Hay - Puterea este in tine 0.7 '{DezvoltarePersonala}.docx
Louise L. Hay - Totul este bine 1.0 '{Spiritualitate}.docx
Louise L. Hay - Vindeca-ti corpul 0.5 '{Spiritualitate}.docx

./Louise Lawson:
Louise Lawson - Incapatanata ca un catar 0.9 '{Dragoste}.docx
Louise Lawson - Lasa-ma sa te iubesc 0.99 '{Dragoste}.docx

./Louise O'Neill:
Louise O'Neill - Si-a cautat-o 1.0 '{Diverse}.docx

./Louise Penny:
Louise Penny - Locul de unde vine lumina 1.0 '{Thriller}.docx
Louise Penny - Lungul drum spre casa 1.0 '{Thriller}.docx

./Louis Ferdinand Celine:
Louis Ferdinand Celine - Calatorie la capatul noptii 1.0 '{CapasiSpada}.docx
Louis Ferdinand Celine - Moarte pe credit 1.0 '{CapasiSpada}.docx

./Louis Hemon:
Louis Hemon - Maria Chapdelaine 1.0 '{Literatura}.docx

./Louis Henri Boussenard:
Louis Henri Boussenard - Capitanul Casse Cou 1.0 '{AventuraTineret}.docx

./Lou Marinoff:
Lou Marinoff - Inghite Platon, nu Prozac 1.0 '{Filozofie}.docx

./Luca D`Andrea:
Luca D`Andrea - Esenta raului 1.0 '{Thriller}.docx

./Luca di Fulvio:
Luca di Fulvio - Copilul care a gasit Soarele noaptea 1.0 '{Literatura}.docx
Luca di Fulvio - Fata care a atins cerul 1.0 '{Literatura}.docx

./Luc Besson:
Luc Besson - Artur si Minimosii - V1 Artur si minimosii 1.0 '{Tineret}.docx
Luc Besson - Artur si Minimosii - V2 Arthur si cetatea interzisa 1.0 '{Tineret}.docx
Luc Besson - Artur si Minimosii - V3 Arthur si razbunarea lui Maltazar 1.0 '{Tineret}.docx
Luc Besson - Artur si Minimosii - V4 Arthur si razboiul celor doua lumi 0.9 '{Tineret}.docx

./Lucia Demetrius:
Lucia Demetrius - Te iubesc viata 0.6 '{Literatura}.docx

./Lucian Blaga:
Lucian Blaga - La cumpana apelor 0.99 '{Versuri}.docx
Lucian Blaga - La curtile dorului 0.99 '{Versuri}.docx
Lucian Blaga - Lauda somnului 0.99 '{Versuri}.docx
Lucian Blaga - Mesterul Manole 1.0 '{Teatru}.docx
Lucian Blaga - Nebanuitele trepte 0.99 '{Versuri}.docx
Lucian Blaga - Pasii profetului 0.99 '{Versuri}.docx
Lucian Blaga - Poemele luminii 0.99 '{Versuri}.docx
Lucian Blaga - Trilogia Culturii - V1 Orizont si stil 0.7 '{ClasicRo}.docx
Lucian Blaga - Trilogia Culturii - V2 Spatiul mioritic 0.7 '{ClasicRo}.docx
Lucian Blaga - Trilogia Culturii - V3 Geneza metaforei 0.7 '{ClasicRo}.docx
Lucian Blaga - Varsta de fier 0.99 '{Versuri}.docx

./Lucian Boia:
Lucian Boia - Cum am trecut prin comunism 0.9 '{ClasicRo}.docx
Lucian Boia - De ce este Romania altfel 1.0 '{ClasicRo}.docx
Lucian Boia - Doua secole de mitologie nationala 1.0 '{ClasicRo}.docx
Lucian Boia - Elita intelectuala romaneasca intre 1930 si 1950 1.0 '{ClasicRo}.docx
Lucian Boia - Eugen Brote 1.0 '{ClasicRo}.docx
Lucian Boia - Evolutia istoriografiei romane 1.0 '{ClasicRo}.docx
Lucian Boia - Franta, hegemonie sau declin 1.0 '{ClasicRo}.docx
Lucian Boia - Germanofilii. Elita intelectuala romaneasca in anii primului razboi mondial 2.0 '{ClasicRo}.docx
Lucian Boia - Intre inger si fiara. Mitul omului diferit din antichitate pana in zilele noastre 1.0 '{ClasicRo}.docx
Lucian Boia - Istorie si mit in constiinta romaneasca 1.0 '{ClasicRo}.docx
Lucian Boia - Jocul cu trecutul. Istoria intre adevar si fictiune 1.0 '{ClasicRo}.docx
Lucian Boia - Jules Verne. Paradoxurile unui mit 1.0 '{ClasicRo}.docx
Lucian Boia - Mitologia stiintifica a comunismului 1.0 '{ClasicRo}.docx
Lucian Boia - Mitul democratiei 1.0 '{ClasicRo}.docx
Lucian Boia - Mitul longevitatii sau cum sa traim 200 de ani 0.8 '{ClasicRo}.docx
Lucian Boia - Napoleon III cel neiubit 1.0 '{ClasicRo}.docx
Lucian Boia - Occidentul, o interpretare istorica 1.0 '{ClasicRo}.docx
Lucian Boia - Omul si clima. Teorii, scenarii, psihoze 1.0 '{ClasicRo}.docx
Lucian Boia - Pentru o istorie a imaginarului 1.0 '{ClasicRo}.docx
Lucian Boia - Romania, tara de frontiera a Europei 1.0 '{ClasicRo}.docx
Lucian Boia - Sfarsitul lumii. o istorie fara sfarsit 1.0 '{ClasicRo}.docx
Lucian Boia - Tinerete fara batranete. Imaginarul longevitatii din antichitate pana astazi 1.0 '{ClasicRo}.docx
Lucian Boia - Tragedia Germaniei 1914-1945 1.0 '{ClasicRo}.docx

./Lucian Cursaru:
Lucian Cursaru - Ocrotiti pasarea Paradis 1.0 '{Literatura}.docx

./Lucian Dan Teodorovici:
Lucian Dan Teodorovici - 96-00 povestiri 0.99 '{ProzaScurta}.docx
Lucian Dan Teodorovici - Cu putin timp inaintea coborarii extraterestrilor printre noi 1.0 '{Diverse}.docx

./Lucian Ionica:
Lucian Ionica - Imaginea vizuala 0.8 '{Diverse}.docx
Lucian Ionica - La marginea orasului 0.7 '{SF}.docx
Lucian Ionica - Nasturii 0.8 '{SF}.docx
Lucian Ionica - Nenumarate sanse 0.7 '{Diverse}.docx
Lucian Ionica - Tehnicianul si bioritmul 0.5 '{SF}.docx
Lucian Ionica - Tehnicianul si telefonul 0.7 '{SF}.docx
Lucian Ionica - Ziua confuza 1.0 '{SF}.docx

./Lucian Mares:
Lucian Mares - Capcana timpului 0.8 '{SF}.docx
Lucian Mares - Jurnalul raului 0.7 '{Necenzurat}.docx

./Lucian Merisca:
Lucian Merisca - Agentul neantului 0.99 '{SF}.docx
Lucian Merisca - Noaptea lui 0 si 1 0.99 '{SF}.docx
Lucian Merisca - Salasul 0.9 '{Diverse}.docx

./Lucian Penescu:
Lucian Penescu - Diamante in umbra 2.0 '{Politista}.docx
Lucian Penescu - Jocul noptii 1.0 '{Literatura}.docx
Lucian Penescu - O floare pentru eternitate 1.0 '{Literatura}.docx
Lucian Penescu - Taina 1.0 '{Literatura}.docx
Lucian Penescu - Un infinit de emotii 1.0 '{Literatura}.docx

./Lucian Petrescu:
Lucian Petrescu - Cameleon 0.99 '{Literatura}.docx

./Lucian Sava:
Lucian Sava - Apoka si Kalipta 0.99 '{SF}.docx
Lucian Sava - Capitole 0.99 '{SF}.docx

./Lucian Strochi:
Lucian Strochi - Gambit 1.0 '{Politista}.docx

./Lucian Vasile Szabo:
Lucian Vasile Szabo - Cronia 0.99 '{SF}.docx
Lucian Vasile Szabo - Iubita de la miezul noptii 0.9 '{SF}.docx
Lucian Vasile Szabo - Martorul ocular 0.7 '{SF}.docx
Lucian Vasile Szabo - Pe orbita creierului nostru 0.99 '{SF}.docx

./Lucia Robitu:
Lucia Robitu - Parintele 0.99 '{SF}.docx

./Lucia Verona:
Lucia Verona - Carte de povesti 0.99 '{BasmesiPovesti}.docx

./Lucie Palma:
Lucie Palma - Amantii din Naxos 0.9 '{Dragoste}.docx

./Lucilla Andrews:
Lucilla Andrews - Vremea tamaduirii 1.0 '{Romance}.docx

./Lucinda Hare:
Lucinda Hare - Imblanzitorul dragonilor 1.0 '{SF}.docx

./Lucinda Riley:
Lucinda Riley - Secretul Helenei 1.0 '{Dragoste}.docx
Lucinda Riley - Secretul orhideei 1.0 '{Dragoste}.docx
Lucinda Riley - Trandafirul noptii 1.0 '{Literatura}.docx

./Lucius Apuleius:
Lucius Apuleius - Magarul de aur 1.0 '{ClasicSt}.docx

./Lucius Shepard:
Lucius Shepard - La frontiera 0.99 '{SF}.docx
Lucius Shepard - Sfarsitul Pamantului 1.0 '{SF}.docx
Lucius Shepard - Skull City 0.99 '{SF}.docx
Lucius Shepard - Vanatorul de jaguari 1.0 '{SF}.docx

./Lucy Keating:
Lucy Keating - Arta visarii 0.99 '{Tineret}.docx

./Lucy Walker:
Lucy Walker - Adevarata dragoste 0.99 '{Romance}.docx
Lucy Walker - Fata mereu desculta 0.99 '{Romance}.docx
Lucy Walker - Judecatorul de pace 0.99 '{Dragoste}.docx
Lucy Walker - Sange irlandez 0.9 '{Dragoste}.docx
Lucy Walker - Sunetul cornului 0.9 '{Dragoste}.docx

./Ludmila Filipova:
Ludmila Filipova - Labirintul de pergament 0.99 '{AventuraIstorica}.docx

./Ludmila Ulitkaia:
Ludmila Ulitkaia - Al dumneavoastra sincer, Surik 1.0 '{Literatura}.docx
Ludmila Ulitkaia - Daniel Stein, traducator 1.0 '{Literatura}.docx
Ludmila Ulitkaia - Soniecika 1.0 '{Literatura}.docx

./Ludovic Roman:
Ludovic Roman - Aventurile farfuriei zburatoare V1 1.0 '{ClubulTemerarilor}.docx
Ludovic Roman - Aventurile farfuriei zburatoare V2 1.0 '{ClubulTemerarilor}.docx
Ludovic Roman - Racheta alba 0.9 '{AventuraTineret}.docx
Ludovic Roman - Stejara 1.0 '{AventuraTineret}.docx

./Ludwig Fels:
Ludwig Fels - Pacatele saraciei 0.9 '{Literatura}.docx

./Ludwig Fulda:
Ludwig Fulda - Prostul 1.0 '{Teatru}.docx

./Ludwig Wittgenstein:
Ludwig Wittgenstein - Caietul albastru 0.99 '{Filozofie}.docx
Ludwig Wittgenstein - Despre certitudine 0.99 '{Filozofie}.docx

./Luigi Accattoli:
Luigi Accattoli - Karol Woytila, omul sfarsitului de mileniu 0.99 '{Religie}.docx

./Luigi Capuana:
Luigi Capuana - Marchizul de Roccaverdina 0.6 '{Literatura}.docx

./Luigi Menghini:
Luigi Menghini - Asediul 1.0 '{SF}.docx
Luigi Menghini - Azeneg 1.9 '{SF}.docx
Luigi Menghini - Mercenarii ratiunii 1.9 '{SF}.docx
Luigi Menghini - Reactie in lant 1.0 '{SF}.docx

./Luigi Pirandello:
Luigi Pirandello - Exclusa 2.0 '{Literatura}.docx
Luigi Pirandello - Unul, nici unul si o suta de mii 6.0 '{Literatura}.docx

./Luigi Pirandello & Giustino Roncella:
Luigi Pirandello & Giustino Roncella - Batrani si tineri 1.0 '{Literatura}.docx

./Luigi Preti:
Luigi Preti - Dilema iubirii 0.9 '{Dragoste}.docx

./Luis Higuera:
Luis Higuera - Autopsia satanei 1.0 '{AventuraIstorica}.docx

./Luis Landero:
Luis Landero - Jocurile varstei tarzii 0.99 '{Literatura}.docx

./Luis Martin Santos:
Luis Martin Santos - Vremea tacerii 2.0 '{Literatura}.docx

./Luis Miguel Rocha:
Luis Miguel Rocha - Vatican - V1 Ultimul papa 1.0 '{Politista}.docx
Luis Miguel Rocha - Vatican - V2 Papa trebuie sa moara 1.0 '{Politista}.docx

./Luis Rogelio Nogueras:
Luis Rogelio Nogueras - Iar daca maine am sa mor 0.9 '{Politista}.docx

./Luis Sepulveda:
Luis Sepulveda - Batranul care citea romane de dragoste 0.99 '{Thriller}.docx
Luis Sepulveda - Jurnalul unui killer sentimental 0.99 '{Thriller}.docx
Luis Sepulveda - Nume de toreador 0.99 '{Thriller}.docx
Luis Sepulveda - Yacare 0.99 '{Thriller}.docx

./Luiza Carol:
Luiza Carol - A opta poarta 0.9 '{Teatru}.docx

./Luke Allnutt:
Luke Allnutt - Al nostru este cerul 1.0 '{Literatura}.docx

./Luminita Logofatu:
Luminita Logofatu - Ghid de educatie sexuala 0.9 '{Psihologie}.docx

./Luong Minh Dang:
Luong Minh Dang - Energia universala sau yoga spirituala si umana 0.6 '{Spiritualitate}.docx

./Lurentiu Cernet:
Lurentiu Cernet - Salt in maine 1.0 '{SF}.docx

./Lu Sin:
Lu Sin - Adevarata poveste a lui AQ 1.0 '{Literatura}.docx

./Lynda La Plante:
Lynda La Plante - Dolly Rawlins - V1 Vaduve 1.0 '{Thriller}.docx

./Lynda Trent:
Lynda Trent - Prieteni nedespartiti 0.6 '{Romance}.docx

./Lynn Austin:
Lynn Austin - Caminul mult visat 0.9 '{Literatura}.docx
Lynn Austin - Fiicele Evei 1.0 '{Literatura}.docx
Lynn Austin - Taramuri ascunse 1.0 '{Literatura}.docx

./Lynn Fairfax:
Lynn Fairfax - In cautarea sufletului pereche 0.99 '{Dragoste}.docx
Lynn Fairfax - Sub protectia iubirii 0.99 '{Dragoste}.docx

./Lynn Mckay:
Lynn Mckay - Am asteptat destul 0.2 '{Dragoste}.docx
Lynn Mckay - In mainile destinului 0.99 '{Dragoste}.docx

./Lynn Picknett & Clive Prince:
Lynn Picknett & Clive Prince - Misterul templierilor 1.0 '{MistersiStiinta}.docx

./Lynn Sholes:
Lynn Sholes - Conspiratia Graalului 1.0 '{AventuraIstorica}.docx
Lynn Sholes - Ultimul secret 1.0 '{AventuraIstorica}.docx

./Lyssa Royal:
Lyssa Royal - Prisma din Lira 0.99 '{Spiritualitate}.docx
```

